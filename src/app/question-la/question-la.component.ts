import { Component, OnInit , Input } from '@angular/core';

@Component({
  selector: 'app-question-la',
  templateUrl: './question-la.component.html',
  styleUrls: ['./question-la.component.css']
})
export class QuestionLAComponent implements OnInit {
  @Input() ques: {
     questionId: string,
    questionPara:   string  ,
    answerOption:  [{
      option1: string,
      option2: string,
      option3: string,
      option4: string
    }],
    correctAnswer: [{text: string, type: string}],
    dateCreated:   string,
    flagCount:     number,
     grades:        [{value: string}],
    hiddenTags:    [{tagType: string,
      value: string
    }],
     isAdvanced:     boolean,
      pathToCreateQuestionPage: string,
     questionImagesUrl: string,
    questionStatus: string,
     questionText: string,
     questionType: string,
      questionViews: number,
    upVotes: number,
   visibleTags: [{
      tagType: string,
      value: string
    }]
  };
    constructor() { }

  ngOnInit() {
  }
}
