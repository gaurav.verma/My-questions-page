import {Component, Input, OnInit} from '@angular/core';


@Component({
  selector: 'app-question-mcqimage',
  templateUrl: './question-mcqimage.component.html',
  styleUrls: ['./question-mcqimage.component.css']
})
export class QuestionMCQImageComponent implements OnInit {
  @Input() ques: {
    questionId: string,
    questionPara:   string  ,
    answerOption:  {
      option1: string,
      option2: string,
      option3: string,
      option4: string
    },
    correctOption: string,
    dateCreated:   string,
    flagCount:     number,
    grades:        [{value: string}],
    hiddenTags:    [{tagType: string,
      value: string
    }],
    isAdvanced:     boolean,
    pathToCreateQuestionPage: string,
    questionImagesUrl: string,
    questionStatus: string,
    questionText: string,
    questionType: string,
    questionViews: number,
    upVotes: number,
    visibleTags: [{
      tagType: string,
      value: string
    }]
  };

  constructor() { }

  ngOnInit() {
  }

}
