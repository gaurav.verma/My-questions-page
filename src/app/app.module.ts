import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {HeaderComponent} from './header/header.component';
import {QuestionLAComponent} from './question-la/question-la.component';
import { QuestionMCQComponent } from './question-mcq/question-mcq.component';
import { QuestionMCQImageComponent } from './question-mcqimage/question-mcqimage.component';
import { QuestionVSAComponent } from './question-vsa/question-vsa.component';
import { QuestionMPComponent } from './question-mp/question-mp.component';


import { AngularFireModule } from "angularfire2";
import { firebaseConfig} from "../environments/firebase.config";
import {HttpModule} from "@angular/http";
import {AngularFireDatabaseModule} from "angularfire2/database";

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    QuestionLAComponent,
    QuestionMCQComponent,
    QuestionMCQImageComponent,
    QuestionVSAComponent,
    QuestionMPComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
