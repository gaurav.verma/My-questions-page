import { Component , Output } from '@angular/core';
import { AngularFireAuthModule } from 'angularfire2/auth';


import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import {database, initializeApp} from "firebase/app";
import {firebaseConfig} from "../environments/firebase.config";
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent {
  question: any= [];
  constructor(private af: AngularFireDatabase){
   const courese$: FirebaseListObservable<any> = af.list('user/JbLadA4e5pbs1fPkbu32UhbwMbg2/myQuestions/');
   courese$.subscribe(
     val => this.question.push(val)
   );

  }


}
