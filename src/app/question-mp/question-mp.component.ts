import {Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-question-mp',
  templateUrl: './question-mp.component.html',
  styleUrls: ['./question-mp.component.css']
})

export class QuestionMPComponent implements OnInit {
  answerStyle;
  @Input() ques: {
    nrQuestionId: string,
    questionPara:   string  ,
    answerOption:  {
      option1: string,
      option2: string,
      option3: string,
      option4: string
    },
    correctOption: string,
    dateCreated:   string,
    subQuestions: any,
    flagCount:     number,
    grades:        [{value: string}],
    hiddenTags:    [{tagType: string,
      value: string
    }],
    isAdvanced:     boolean,
    pathToCreateQuestionPage: string,
    questionImagesUrl: string,
    questionStatus: string,
    questionText: string,
    questionType: string,
    questionViews: number,
    upVotes: number,
    visibleTags: [{
      tagType: string,
      value: string
    }]
  };

  ngOnInit() {
    console.log(this.ques.subQuestions);
  }
getFont1() {
    return this.ques.correctOption === '1' ? 'green' : 'black';
}
  getFont2() {
    return this.ques.correctOption === '2' ? 'green' : 'black';
  }
  getFont3() {
    return this.ques.correctOption === '3' ? 'green' : 'black';
  }
  getFont4() {
    return this.ques.correctOption === '4' ? 'green' : 'black';
  }

}
