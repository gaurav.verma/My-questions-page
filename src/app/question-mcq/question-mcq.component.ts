import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-question-mcq',
  templateUrl: './question-mcq.component.html',
  styleUrls: ['./question-mcq.component.css']
})
export class QuestionMCQComponent implements OnInit {
  @Input() ques: {
    questionId: string,
    questionPara:   string  ,
    answerOptions:  {
      option1: string,
      option2: string,
      option3: string,
      option4: string
    },
    correctAnswer: [{text: string, type: string, optionNumber: string}],
    dateCreated:   string,
    flagCount:     number,
    grades:        [{value: string}],
    hiddenTags:    [{tagType: string,
      value: string
    }],
    isAdvanced:     boolean,
    pathToCreateQuestionPage: string,
    questionImagesUrl: string,
    questionStatus: string,
    questionText: string,
    questionType: string,
    questionViews: number,
    upVotes: number,
    visibleTags: [{
      tagType: string,
      value: string
    }]
  };
  ngOnInit() {
  }
  getFont1() {
    if (this.ques.correctAnswer[0].optionNumber !== '') {
      return this.ques.correctAnswer[0].optionNumber === 'a' ? 'green' : 'black';
    }
    if (this.ques.correctAnswer[0].text !== '') {
      return this.ques.correctAnswer[0].text === this.ques.answerOptions.option1 ? 'green' : 'black';
    }
    }
  getFont2() {
    if (this.ques.correctAnswer[0].optionNumber !== '') {
      return this.ques.correctAnswer[0].optionNumber === 'b' ? 'green' : 'black';
    }
    if (this.ques.correctAnswer[0].text !== '') {
      return this.ques.correctAnswer[0].text === this.ques.answerOptions.option2 ? 'green' : 'black';
    }
  }
  getFont3() {
    if (this.ques.correctAnswer[0].optionNumber !== '') {
      return this.ques.correctAnswer[0].optionNumber === 'c' ? 'green' : 'black';
    }
    if (this.ques.correctAnswer[0].text !== '') {
      return this.ques.correctAnswer[0].text === this.ques.answerOptions.option3 ? 'green' : 'black';
    }
  }
  getFont4() {
    if (this.ques.correctAnswer[0].optionNumber !== '') {
      return this.ques.correctAnswer[0].optionNumber === 'd' ? 'green' : 'black';
    }
    if (this.ques.correctAnswer[0].text !== '') {
      return this.ques.correctAnswer[0].text === this.ques.answerOptions.option4 ? 'green' : 'black';
    }
  }

}
